﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace TicTacToe
{
    public class ComputerPlayer : MonoBehaviour
    {

        public GameController gController;
        public List<PieceDetector> availableTiles;

        enum AIState {waiting,scanning,takingturn}
        private AIState aiState;

        private delegate void AIAction();
        private AIAction aiAction;

        private float thinkTimer; //The timer that ticks when an action is being taken

        List<GameController.Seed> _emptySeeds;


        // Use this for initialization
        void Start()
        {
            FindGameController();
        }



        void FindGameController()
        {
            gController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        }


        void StateChange(AIState state)
        {
            switch (aiState)
            {
                case AIState.waiting:
                    break;
                case AIState.scanning:
                    aiAction = ScanBoard;
                    break;
                case AIState.takingturn:
                    break;
            }
        }

        private void ScanBoard()
        {
            thinkTimer -= Time.fixedDeltaTime;
            if(thinkTimer < 0)
            {
                StateChange(AIState.takingturn);
            }
            foreach(GameController.Seed cell in gController.cells)
            {
                if(cell == GameController.Seed.EMPTY)
                {
                    _emptySeeds.Add(cell);
                }
            }
        }

        void FixedUpdate()
        {
            if (aiAction != null)
                aiAction();    
        }
    }
}
