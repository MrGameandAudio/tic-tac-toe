﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TestCameraShake : MonoBehaviour {

    public Camera mainCamera;
	// Use this for initialization
	void Start () {
        DOTween.Init(false, true, LogBehaviour.ErrorsOnly);

        mainCamera.transform.DOShakePosition(10f, 5f, 10, 90, false);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
