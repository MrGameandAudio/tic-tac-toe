﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Generates a grid of prefabs based on the specifications fed to it from either the inspector or another script.
/// </summary>
public class GridScript : MonoBehaviour {

	#region Objects
	GameObject[,] board;
	public GameObject prefab = null;
	#endregion

	#region Variables
	public int width = 0;
	public int height = 0;
	#endregion



	void Start ()
	{
		board = new GameObject[height, width];
		GenerateGrid();
	}

	void GenerateGrid()
	{
		for (int h = 0; h<height; h++)
		{
			for (int w = 0; w<width; w++)
			{
				board[h,w] = (GameObject)Instantiate(prefab, new Vector3(w, h, 0), Quaternion.identity);
				board[h,w].transform.parent = transform;
			}
		}
	}
}
