﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace TicTacToe
{
    public class PieceDetector : MonoBehaviour
    {

        #region GameObjects
        public GameController gameController = null;
        #endregion

        #region GameObjects
        public int index;
        #endregion

        void Start()
        {
            gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        }

        void OnMouseDown()
        {
            if (gameController.isPlaying)
                gameController.SpawnNew(gameObject, index);
        }
    }
}