﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

namespace TicTacToe
{
    public class GameController : MonoBehaviour {

        #region Enums
        public enum Seed { EMPTY, CROSS, NOUGHT };
        public Seed turn;
        #endregion

        #region Objects
        GameObject[] references;
        private GameObject currentBoard;
        public GameObject cross, nought, xBar, oBar;
        public List<GameObject> boardPrefabs;
        ComputerPlayer computerAI;
        [SerializeField]
        private Text winText;
        #endregion

        #region Variables
        public Seed[] cells;
        public bool isPlaying = true;
        Vector2 pos1, pos2;
        System.Random rnd = new System.Random();
        bool isDraw()
        {
            if (whoWon() != Seed.EMPTY)
                return false;
            for (int i = 0; i < 9; i++)
                if (cells[i] == Seed.EMPTY)
                    return false;
            return true;
        }
        Vector2 calCenter()
        {
            float x = (pos1.x + pos2.x) / 2,
                  y = (pos1.y + pos2.y) / 2;
            return new Vector2(x, y);
        }

        int _cellsUsed = 0;
        #endregion

        #region Events
        public UnityEvent gameOverEvent;
        private bool aiPlayer;
        #endregion

        void Start()
        {
            currentBoard = GameObject.FindGameObjectWithTag("Board");
            if (aiPlayer)
            {
                computerAI = GetComponent<ComputerPlayer>();
            }
            InitializeCells();
            CoinFlip();
        }

        public void TurnChange(Seed turnState)
        {
            switch (turnState)
            {
                case Seed.CROSS:
                    turn = Seed.CROSS;
                    break;
                case Seed.NOUGHT:
                    turn = Seed.NOUGHT;
                    break;
                case Seed.EMPTY:
                    turn = Seed.EMPTY;
                    break;
            }
        }

        public void SpawnNew(GameObject obj, int index)
        {
            cells[index] = turn;
            switch (turn)
            {
                case Seed.CROSS:
                    TurnChange(Seed.NOUGHT);
                    references[index] = (GameObject)Instantiate(cross, obj.transform.position, Quaternion.identity);
                    references[index].transform.parent = currentBoard.transform;
                    break;
                case Seed.NOUGHT:
                    TurnChange(Seed.CROSS);
                    references[index] = (GameObject)Instantiate(nought, obj.transform.position, Quaternion.identity);
                    references[index].transform.parent = currentBoard.transform;
                    break;
                case Seed.EMPTY:
                    break;
            }
            if (isDraw() == true)
            {
                turn = Seed.EMPTY;
                isPlaying = false;
            }
            for (int i = 0; i < references.Length; i++)
            {
                if (references[i] == null)
                {
                    _cellsUsed++;
                }
            }
            CheckGameWinner();
            Destroy(obj.gameObject);
            _cellsUsed = 0;
        }

        #region Helper Functions


        public bool boardFull()
        {
            if(_cellsUsed == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        void InitializeCells()
        {
            cells = new Seed[9];
            references = new GameObject[cells.Length];
            for (int i = 0; i < 9; i++)
            {
                cells[i] = Seed.EMPTY;
            }
        }

        void CoinFlip()
        {
            int coinFlip = rnd.Next(0, 2);
            if (coinFlip == 0)
            {
                turn = Seed.CROSS;
            }
            else
            {
                turn = Seed.NOUGHT;
            }

        }



        void CheckGameWinner()
        {
            if (whoWon() == Seed.CROSS)
            {
                StartCoroutine(Wait(0.5f, DrawBar));
                StartCoroutine(Wait(1.5f, gameOverEvent.Invoke)); // not feeling too good about this
                winText.text = "X Wins!";
                isPlaying = false;
            }
            if (whoWon() == Seed.NOUGHT)
            {
                StartCoroutine(Wait(0.5f, DrawBar));
                StartCoroutine(Wait(1.5f, gameOverEvent.Invoke)); //not feeling too good about this
                winText.text = "O Wins!";
                isPlaying = false;
            }
            if (boardFull() && whoWon() == Seed.EMPTY)
            {
                winText.text = "It's a Draw!";
                StartCoroutine(Wait(1.5f, gameOverEvent.Invoke));
            }

        }

        void DrawBar()
        {
            if (whoWon() == Seed.CROSS)
            {
                Vector2 center = calCenter();
                if (pos1.x == pos2.x)
                {
                    Instantiate(xBar, center, Quaternion.identity);
                }
                else if (pos1.y == pos2.y)
                {
                    Instantiate(xBar, center, Quaternion.Euler(0, 0, -90));
                }
                else
                {
                    Debug.Log(pos1.normalized.x);
                    if (pos1.x > 0)
                    {
                        Instantiate(xBar, center, Quaternion.Euler(0, 0, -45));
                    }
                    else
                    {
                        Instantiate(xBar, center, Quaternion.Euler(0, 0, 45));
                    }
                }
            }
            if (whoWon() == Seed.NOUGHT)
            {
                Vector2 center = calCenter();
                if (pos1.x == pos2.x)
                {
                    Instantiate(oBar, center, Quaternion.identity);
                }
                else if (pos1.y == pos2.y)
                {
                    Instantiate(oBar, center, Quaternion.Euler(0, 0, -90));
                }
                else
                {
                    if (pos1.x > 0)
                    {
                        Instantiate(oBar, center, Quaternion.Euler(0f, 0f, -45f));
                    }
                    else
                    {
                        Instantiate(oBar, center, Quaternion.Euler(0f, 0f, 45f));
                    }
                }
            }
        }

        public void ResetBoard()
        {
            Application.LoadLevel("3x3Board");
            //Destroy(currentBoard);
            //currentBoard = Instantiate(boardPrefabs[0]);
            //InitializeCells();
            //CoinFlip();
            //isPlaying = true;
        }

        Seed whoWon()
        {
            Seed flag = Seed.EMPTY;
            if (cells[0] != Seed.EMPTY)
            {
                pos1 = references[0].transform.position;
                if (cells[0] == cells[1] && cells[2] == cells[0])
                {
                    flag = cells[0];
                    pos2 = references[2].transform.position;
                }
                else if (cells[0] == cells[3] && cells[6] == cells[0])
                {
                    flag = cells[0];
                    pos2 = references[6].transform.position;
                }
            }
            if (cells[8] != Seed.EMPTY)
            {
                pos1 = references[8].transform.position;
                if (cells[8] == cells[4] && cells[0] == cells[8])
                {
                    flag = cells[8];
                    pos2 = references[0].transform.position;
                }
                else if (cells[8] == cells[5] && cells[2] == cells[8])
                {
                    flag = cells[8];
                    pos2 = references[2].transform.position;
                }
                else if (cells[8] == cells[7] && cells[6] == cells[8])
                {
                    flag = cells[8];
                    pos2 = references[6].transform.position;
                }
            }
            if (cells[4] != Seed.EMPTY)
            {

                if (cells[4] == cells[1] && cells[7] == cells[4])
                {
                    flag = cells[4];
                    pos1 = references[1].transform.position;
                    pos2 = references[7].transform.position;
                }
                else if (cells[4] == cells[3] && cells[5] == cells[4])
                {
                    flag = cells[4];
                    pos1 = references[3].transform.position;
                    pos2 = references[5].transform.position;
                }
                else if (cells[0] == cells[4] && cells[8] == cells[4])
                {
                    flag = cells[4];
                    pos1 = references[0].transform.position;
                    pos2 = references[8].transform.position;
                }
                else if (cells[4] == cells[2] && cells[6] == cells[2])
                {
                    flag = cells[4];
                    pos1 = references[2].transform.position;
                    pos2 = references[6].transform.position;
                }
            }
            return flag;
        }

        IEnumerator Wait(float seconds, System.Action action)
        {
            yield return new WaitForSeconds(seconds);
            action.Invoke();
        }

        public void SetIsPlaying(bool _isPlaying)
        {
            isPlaying = _isPlaying;
        }
        #endregion
    }
}