﻿using UnityEngine;
using System.Collections;


public class ShowPanels : MonoBehaviour
{
    #region Game Objects
    public GameObject mainMenuPanel;
    public GameObject singlePlayerPanel;
    public GameObject multiPlayerPanel;
    public GameObject optionsPanel;
    public GameObject statsPanel;
    #endregion

    public void SetPanelActive(GameObject panel)
    {
        panel.SetActive(true);
    }

    public void SetPanelInactive(GameObject panel)
    {
        panel.SetActive(false);
    }

    public void SwitchActivePanel(GameObject activePanel,GameObject inactivePanel)
    {
        activePanel.SetActive(false);
        inactivePanel.SetActive(true);
    }
}

