﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {

	public void Load(string levelToLoad)
    {
        Application.LoadLevel(levelToLoad);
    }
    public void LoadAsync(string levelToLoad)
    {
        Application.LoadLevelAsync(levelToLoad);
    }
}
