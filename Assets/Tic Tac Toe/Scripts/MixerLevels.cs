﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;


//This is class is used to allow us to modify the variables associated with the master mixer. If more parameters are exposed please add them here!
public class MixerLevels : MonoBehaviour
{
    public AudioMixer masterMixer;
    public static MixerLevels mixerLevels;

    private float _masterVol;
    private float _musicVol;
    private float _sfxVol;

    void Awake()
    {
        if(mixerLevels == null)
        {
            DontDestroyOnLoad(gameObject);
            mixerLevels = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        GetMixerPreferences();
    }

    #region Audio Properties
    public void setMasterVol(float masterVol)
    {
        masterMixer.SetFloat("masterVol", masterVol);
        _masterVol = masterVol;
    }
    public void setMusicVol(float musicVol)
    {
        masterMixer.SetFloat("musicVol", musicVol);
        _musicVol = musicVol;
    }
    public void setSFXVol(float sfxVol)
    {
        masterMixer.SetFloat("sfxVol", sfxVol);
        _sfxVol = sfxVol;
    }
    #endregion

    #region Preferences Code
    public void SaveMixerPreferences()
    {
        PlayerPrefs.SetFloat("masterVol", _masterVol);
        PlayerPrefs.SetFloat("musicVol", _musicVol);
        PlayerPrefs.SetFloat("sfxVol", _sfxVol);
    }
    public void GetMixerPreferences()
    {
        PlayerPrefs.GetFloat("masterVol", _masterVol);
        PlayerPrefs.GetFloat("musicVol", _musicVol);
        PlayerPrefs.GetFloat("sfxVol", _sfxVol);
        masterMixer.SetFloat("masterVol", _masterVol);
        masterMixer.SetFloat("musicVol", _musicVol);
        masterMixer.SetFloat("sfxVol", _sfxVol);

    }
    #endregion
}
