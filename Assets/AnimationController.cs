﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace TicTacToe
{
    public class AnimationController : MonoBehaviour
    {

        #region Game Objects
        public GameObject board, scoreBoard, scoreXPiece, scoreOPiece;
        private GameController gameController = null;
        #endregion

        #region Variables
        #endregion

        #region Components
        #endregion

        #region enums
        public enum Seed { EMPTY, CROSS, NOUGHT };
        public Seed turn;
        #endregion

        // Use this for initialization
        void Start()
        {
            InitializeController();
            StartupAnimation();
        }

        //grab components needed to run controller.
        void InitializeController()
        {
            gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            CheckTurn();
        }

        void CheckTurn()
        {
            if (gameController.turn == GameController.Seed.CROSS)
            {
                scoreXPiece.transform.DOScale(1.5f, .5f);
                scoreOPiece.transform.DOScale(1, .5f);
            }
            else if (gameController.turn == GameController.Seed.NOUGHT)
            {
                scoreOPiece.transform.DOScale(1.5f, .5f);
                scoreXPiece.transform.DOScale(1, .5f);
            }

        }

        void StartupAnimation()
        {
            Sequence startUpSequence = DOTween.Sequence();
            //startUpSequence.Append(board.transform.DOMove(new Vector3(0, 0, 0), 1f, false).SetEase(Ease.OutBack));
            startUpSequence.Join(scoreBoard.transform.DOLocalMoveY(342f, 0.3f, false).SetDelay(0.2f).SetEase(Ease.OutBack));
        }
    }
}
